/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 *
 * This document gives some background instructions for those
 * unfortunate enough to be behind extremely restrictive
 * proxies or firewalls.  The idea is to briefly explain a
 * possible way around these needless restrictions as well
 * as how they relate to Arabeyes and the work related there-in.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

GNU httptunnel is best used as an application which allows people
behind restrictive proxies or firewalls (for whatever reason) to
access the internet given web access is available.  In other words,
if you can "surf" the net and can access various public websites,
then you'll be able to access the likes of CVS, ssh, among others.
Httptunnel is also less commonly used as a randomizer of traffic in
the sense that someone can use it to bounce his/her web traffic off
of various public anonymous proxy servers to not have one's IP address
be logged by the remote server/website.

GNU httptunnel's website describes it as,

    ''GNU httptunnel creates a bidirectional virtual
      data connection tunneled in HTTP requests.  The
      HTTP requests can be sent via an HTTP proxy if
      so desired. This can be useful for users behind
      restrictive firewalls. If WWW access is allowed
      through a HTTP proxy, it's possible to use
      httptunnel and, say, telnet or PPP to connect to
      a computer outside the firewall.''

GNU httptunnel uses a server/client approach to attain its goal.
The way it works is rather simple.  Assume you have two hosts
or machines - Local.Host and Remote.Host.  The Local.Host
will wrap all its requests to a certain port as HTTP packets
and send them to a Remote.Host.  Once the Remote.Host receives
the packet it unwraps the transmittal and forwards its contents
as appropriate (based on its setup).  The Remote-to-Local traffic
follows the same mechanism as its counterpart.

Example-1 (ssh):

Assume a user is behind a very restrictive proxy yet ssh access
is needed,

 On Remote host (its IP address is 10.8.4.1):
    $ hts -F localhost:22 4422

  Meaning: Run the httptunnel server (thus 's') and forward all
           traffic received from port-4422 to the localhost
           (ie. keep it local) to port-22

 On Local host:
    $ htc -A user:pass -P proxy.myhost.com:8080 -F 3322 10.8.4.1:4422
    $ ssh -p 3322 localhost

  Meaning: Run the httptunnel client (thus 'c') and forward all
           traffic/packets received from localhost port-3322 to
           specified IP address port-4422.
  Meaning: Run the ssh application and tell it to send all its
           packets to port-3322 (for it to be intercepted and
           forwarded)

  Note: The numbers used above (4422 and 3322) are arbitrary.

  Note: The -A, -P options are simply there to login to the internal
        local proxy (if necessary).

Example-2 (cvs):

 On Remote host (its IP address is 10.8.4.1):
    $ hts -F localhost:2401 4401

 On Local host:
    $ htc -A user:pass -P proxy.myhost.com:8080 -F 2401 10.8.4.1:4401
    $ setenv CVS_RSH ssh
    $ setenv CVSROOT :pserver:user@localhost:/home/arabeyes/cvs
    $ cvs login
    $ cvs co ... / cvs commit ...

For further info on httptunnel's options, please consult its manpage.

Due to a limitation in httptunnel's code a single tunnel can only
handle a single client.  In other words if you want/need to have
2 people (or 2 connections) to be present at any one time on the
same remote host, you'll need to run 2 (or more) instances of 'hts'
using different receive ports.

As far as Arabeyes is concerned, and due to its pledge to do all that
it can to ease access to its volunteers and developers, the following
simple pointers are noted (with regard to CVS only),

 + If you are behind a restrictive internet connection (ie. proxy)
   and you've already received your CVS account, mail 'support' your
   request for a tunnel (make sure you really need this apriori).

 + Arabeyes' staff will setup a dedicated 'hts' process along with
   a port number to serve your needs.

Do please inform Arabeyes if your tunnel is no longer a necessity
(once assigned) for it can be then rededicated to someone that might
need it.

Enjoy - yet another Arabeyes service to help all its volunteers :-)
