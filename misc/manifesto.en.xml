<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
  "/usr/share/sgml/docbook/dtd/xml/4.2/docbookx.dtd" []>
<!-- $Id$ -->
<article>
  <articleinfo>
    <title>The Arabeyes Manifesto</title>
    <author>
      <firstname>Mohammed</firstname>
      <surname>Elzubeir</surname>
      <affiliation>
        <orgname>Arabeyes Project</orgname>
        <address><email>contact (at) arabeyes dot org</email></address>
      </affiliation>
    </author>
    <address><email>elzubeir (at) arabeyes dot org</email></address>
    <pubdate>$Revision$</pubdate>
    <abstract><para>
    This document outlines the ideas and philosophies behind the Arabeyes
    project.
    </para>
    </abstract>
  </articleinfo>


<sect1 id="intro"><title>Introduction</title>
  <para>
Arabeyes<footnote><para>Arabeyes as in Arabize :-)</para></footnote> is an 
Open Source voluntary <emphasis>Meta-Project</emphasis><footnote><para>A 
collection of programs complementing one another, like the GNOME project for 
instance.</para></footnote> aimed at Arabizing the Unix environment by 
providing the necessary interface, tools, and underlying engine(s). It is aimed
at providing a centralized knowledge-base of anything that deals with Arabic 
linux/unix software as well as a means to contribute to the standardization 
effort through various means.
  </para>
  <para>
The lack of standards in the world of Arabic Computing is of great concern to
the Arabeyes project. In fact, a quick look at the Arabic keyboards on the
Internet would give a clear indication of the severeness of the problem. 
There are at least five different keyboard mappings and no clear standard.
Demanding standards, creating and implementing them are all items that fit
under Arabeyes' umbrella and charter.
Standards nowadays encompass many details which have to be sifted though and
studied in great detail in order to start comprehending their affects on
the Arabization effort.  Arabeyes is committed to the assistance of that
effort as well as in putting forth suggestions and presenting solutions
pertaining to our communal needs via open discussion of the facts and
effects.
  </para>
  <para>
Arabization projects are currently scattered across the Internet with no
means of integration and/or cooperation. Arabeyes is meant to fill that gap,
and become a central location for all Arabization efforts. That includes 
hosting and tracking all available Linux/Unix Arabic software.
Arabeyes also aims to develop its own tools and utilities which are necessary
to provide for a basic computer interface (at both the console and GUI 
levels).
  </para>
  <para>
Arabeyes also aims to present an open forum for discussion in order to
"spread" the knowledge and give an opportunity for the "haves" to share with
the "have-nots". Arabeyes' utilization of CVS (Concurrent Versioning System) 
is meant to prolong the lifecycle and existance of the projects as well as to 
attract and incite onlookers and developers to jump-in and modify code (direct
involvement).
  </para>
  <para> 
Raising computer and Linux/Unix awareness in the Arab world is one of the
reasons Arabeyes exists today. The up-and-coming student generation needs to 
know of the existence of Arabeyes (and the Open Source movement) in order for 
them to acquire the various necessary tools as well as to give them a forum to 
learn, teach and participate. We are also raising awareness within the Open 
Source community about Arabization issues.
  </para>
  <para>
Finally, Arabeyes seeks to garner the respect of their peers (other Open
Source projects) and their communities.
  </para>
</sect1>

<sect1 id="how"><title>How We Want to Do It</title>
  <para>
This is not the first attempt to bring Arabic support to the Unix environment.
However, with better planning, excursion and a bit of ingenuity Arabeyes
is planned to extend well beyond past successes and avoid previous failures
and mistakes.  Arabeyes, its contents and projects will live-on
irrespective of maintainer, location or sponsorship -- that in itself is
a shift in previous paradigms.  
  </para>
  <sect2 id="unix"><title>Unix and The Free Software Foundation</title>
    <para>
Unix systems have great potential in the fields of stability, scalability, 
and speedy development cycles. In fact, a quick look at the history of the 
Linux operating system would show that in one decade it was able to present
itself as a serious system, developed by computer enthusiasts and 
professionals alike, over the Internet.
    </para>
    <para>
The Free Software Foundation (launched in 1984) provides a complete Unix-like
operating system which is <emphasis>free</emphasis>. The concept of free 
software is a new one to the Arab world (in fact, the compliance with 
international copyright laws is relatively new worldwide). Free Software will 
allow for accelerated development (which has not been possible with in-house 
closed-source projects), and a transparency which will give incentive to excel 
and compete productively.
    </para>
    <para>
The costs endured by educational institutes could be drastically cut by
using better and free software. This provides for a good foundation to
more secure systems (not limited to schools) - as the dependence on
foreign software is kept to a bare minimum. 
    </para>
  </sect2>

  <sect2 id="development"><title>Development Style</title>
    <para>
The Arabeyes project complies with the Open Source concepts and ideals.
In fact, the adoption of the open source licenses is a fundamental part
of the project's philosophy.
    </para>
    <para>
The utilization of a CVS Repository (which is world-accessible) provides
developers with:
    </para>
    <itemizedlist>
      <listitem><para>
An easy way of maintaining code and other miscellaneous project items.
      </para></listitem>
      <listitem><para>
Adding and improving upon existing work concurrently.
      </para></listitem>
      <listitem><para>
Ensuring a long lifespan for the project, regardless of commitment of the 
oringinal developers.
      </para></listitem>
    </itemizedlist>
    <para>
Mailing list(s) are setup so questions and related issues can be addressed
and discussed. General Arabic Linux/Unix FAQ's<footnote><para>FAQ stands for
Frequently Asked Questions.</para></footnote> and specific-project FAQ's
are provided to serve as a first-place-to-look for those with questions.
    </para>
    <para>
Decisions are taken by the 'core' team. This <emphasis>core</emphasis> 
group is made up of key individuals who are contributing to the project on a 
multitude of ways.
    </para>
  </sect2>
  
  <sect2 id="approach"><title>Arabization Approach</title>
    <para>
It is hard to take a linear approach when it comes to Arabization. Some of 
the important things that need to be tackled are:
    </para>
    <itemizedlist>
      <listitem><para>Fonts</para><para>
The wide variation of fonts and lack of compliance with existing 
standards only makes the Arabization process more challenging. The issue of 
fonts must be addressed seriously and quickly.
      </para></listitem>
      <listitem><para>Libraries</para><para>
GTK/Pango and Qt provide varying Arabic support. Currently Qt3 seems to be 
promising. 
      </para></listitem>
      <listitem><para>Console/Interface</para><para>
The console level, <application>Akka</application> provides Arabic support to 
input/output in Arabic. This allows for use under the lynx browser (text-based 
web browser), and other minimal console usages.
      </para></listitem>
      <listitem><para>Mail/Chat/Spell/other Applications</para><para>
Basic applications will have to be addressed to round a person's daily needs.
      </para></listitem>
      <listitem><para>Desktop (GUI)</para><para>
The desktop environment is envisioned as the most challenging
and least "must-have" chain in the link.  Gnome/KDE/etc arabization 
will be addressed as a by-product of all the underlying work that
needs to happen to augment the list above. 
      </para></listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="volunteers"><title>Volunteers and Sponsors</title>
    <para>
Since we rely on voluntary work, financial sponsorship will go directly
towards the project and not individuals. All donations  (if and
when) will be strictly used on software/hardware on a need-basis for 
Arabeyes projects. Monetary exchanges will be made available to donors
upon request.
    </para>
  </sect2>

</sect1>

<sect1 id="goals"><title>Goals</title>
  <sect2 id="longterm-goals"><title>Long Term</title>
    <orderedlist>
      <listitem><para>
Increase the number of competent Arab programmers/developers. This 
will increase the availability and enlarge the pool of employment
candidates in the future.
      </para></listitem>
      <listitem><para>
Generate corporate interest in the use and development of Arabeyes  and Unix.
      </para></listitem>
      <listitem><para>
Motivate programmers-to-be to develop native Arabic software
      </para>
        <itemizedlist>
          <listitem><para>
            Target younger generations (schools/universities)
          </para></listitem><listitem><para>
            Professionals
          </para></listitem>
        </itemizedlist>
      </listitem>
      <listitem><para>
Retain knowledge base:</para><para>
Knowledge will remain within the Arabeyes project, and accumulate as more (and 
different) people contribute, regardless of how long they remain committed to 
the project's development.
      </para></listitem>
      <listitem><para>
Spread free software awareness in the Arab world
      </para>
        <orderedlist>
          <listitem><para>
            Re-introduce concepts of <emphasis>volunteerism</emphasis> and 
            sharing within a different context (computers)
          </para></listitem><listitem><para>
            Campaign for use and development of free software 
          </para></listitem>
        </orderedlist>
      </listitem>
      <listitem><para>
Arabic Linux/Unix distributions money making opportunities - in distribution, 
product support, and sales of technical books.
      </para></listitem>
    </orderedlist>
  </sect2>
  
  <sect2 id="shortterm-goals"><title>Short Term</title>
    <orderedlist>
      <listitem><para>
Provide a usable Arabic Unix interface for basic computing needs
      </para></listitem>
      <listitem><para>
Become a central hub for Arabization efforts
      </para></listitem>
      <listitem><para>
Raise awareness,  
      </para>
        <itemizedlist>
          <listitem><para>about this project</para></listitem>
          <listitem><para>about the need of external support</para></listitem>
        </itemizedlist>
      </listitem>
      <listitem><para>
Recruit developers, designers, etc.
      </para></listitem>
      <listitem><para>
Find sponsorship in the form of moral, technical, and/or financial support.
      </para></listitem>
    </orderedlist>
  </sect2>

</sect1>

<sect1 id="benefits"><title>Benefits</title>

  <itemizedlist>
    <listitem><para>Average user:</para><para>
For the average user, Arabeyes will provide a usable productive system in which
he/she can use Arabic and Arabized software.
    </para></listitem>
    <listitem><para>Small-company:</para><para>
For the small start-up company, a free and secure system will provide for a 
good financial decision, without the worries of restrictive commercial 
licenses.
    </para></listitem>
    <listitem><para>Big corporations:</para><para>
The big corporations have a lot to gain. Other than finding a cheaper 
alternative to proprietary closed-source commercial software, they will find 
an increasing number of competent programmers and developers who are well 
acquainted with the issues relating to Arabic software.
    </para></listitem>
    <listitem><para>Governmental offices:</para><para>
Governmental offices and establishments have all the benefits of the 
big corporations with an edge when it comes to security. In-house 
development of security-related applications and sensitive data is 
more feasible with a larger number of Arab developers. 
    </para></listitem>
    <listitem><para>Arab programmer society:</para><para>
The Arab programmer society can find a means to communicate, share, 
teach and learn from one another. The currently fragmented society 
will find it more productive when all the efforts are combined. It 
will also benefit from having a larger audience, and a louder voice.
    </para></listitem>
    <listitem><para>Arab community in general:</para><para>
The community will benefit by being able to use (with the option of improving 
upon) free native Arabic software with no license restrictions of use. This 
will provide the foundation for an accelerated development in free Open Source 
Arabic software and in the computer industry in general. Arabeyes will also 
generate hope and propel Arab aspirations to excel in technological feats.
    </para></listitem>
  </itemizedlist>
</sect1>
 

<sect1 id="you-can"><title>What YOU Can Do</title>
  <sect2 id="documentation"><title>Documentation</title>
    <para>
Documentation is one of the most important aspects of any project. In fact, it 
may be more important than the code. That being said, we have two parallel 
documentation efforts which you can pick from:
    </para>
    <itemizedlist>
      <listitem><para>
Translation - there is a vast number of Linux/Unix documents, FAQ's, and 
HOWTO's that need to be translated. If you think you would like to translate, 
and know both Arabic and English with moderate computer knowledge then this 
may be for you. 
      </para></listitem>
      <listitem><para>
Current Projects - Arabeyes projects will need proper documentation too. This 
can be done in both languages. So, if you don't feel that you have the edge of 
both languages, this may be for you.
      </para></listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="participation"><title>Participation</title>
    <para>
The simple act of participating by voicing your opinion, trying a product, 
and/or reading about others' experiences is a form of help that we dearly 
appreciate. Participation is a key factor which 
may determine the success of Arabeyes. The more you learn, the more you 
can help others as they learn. Utilize our mailing list(s) for that purpose 
and don't hesitate to ask.
    </para>
  </sect2>
  
  <sect2 id="develop"><title>Development</title>
    <para>
We are always looking for competent developers and programmers. Students 
are of particular interest to Arabeyes, as they carry the curiosity and
enthusiasm both of which we need. Computer enthusiasts and professionals
are also needed for their experience and expertise. If you feel that you 
have the skills then you definitely have a place in our team.
    </para>
  </sect2>

  <sect2 id="pressure"><title>Organized Pressure</title>
    <para>
Large existing Open Source projects need to be pressured in an 
<emphasis>organized</emphasis> manner to address the issues of Arabization. By
organized pressure, we mean an organized campaign on newsgroups and
mailing lists asking questions, enquiring about development status, and 
demanding more attention. That of course, <emphasis>must</emphasis> be done in 
a civilized manner with an orchestrated agenda accompanied with discussions on 
mailing list(s) a priori.
    </para>
  </sect2>

  <sect2 id="research"><title>Computing Needs Survey/Research</title>
    <para>
Numbers, numbers, numbers! Statistics of computer usage in the Arab world 
are very important to present real numbers when confronting the business 
world. If you have experience in this kind of research and/or have conducted
this type of surveys then we most certainly can benefit from your findings. 
    </para>
  </sect2>
 
  <sect2 id="recruitment"><title>Recruitment</title>
    <para>
If you are not sure you fall under any of those categories, but know someone
who does, spread the word! Your 'word-of-mouth' is much more valuable than 
you may think. If you own and/or operate a web site, please mention us. We 
need all Arab cadre we can summon. 
    </para>
  </sect2>
 
  <sect2 id="sponsorship"><title>Sponsorship</title>
    <para>
If you are a company, educational institution, or even a large corporation 
then you have a lot to offer. We need all forms of support you can offer, 
ranging from (but not limited to) technical, financial, and even moral. 
    </para>
  </sect2>
</sect1>

</article>
